import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-FEdev';
  public userStatus = '';

  // Login result received from login-child
  onLoginResult(message) {
    this.userStatus = message.user + message.message;
  }

    // Login result received from register-child
    onRegisterResult(message) {
      this.userStatus = message.message + message.user;
    }


}
