import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})


export class LoginFormComponent implements OnInit {
  // public elUser: any = document.getElementById('username');

  constructor() { }

  ngOnInit(): void {
  }

  @Input() system: string;

  // Output event emitter
  @Output() loginResult: EventEmitter<any> = new EventEmitter();

  // Button click eventlistener
  onLoginClicked() {
    this.loginResult.emit({
      loggedIn: true,
      user: 'Cash',
      message: ' has successfully logged in'
    });
  }


}
