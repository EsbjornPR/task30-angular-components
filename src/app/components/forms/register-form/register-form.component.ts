import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-register-form',
  templateUrl: './register-form.component.html',
  styleUrls: ['./register-form.component.scss']
})
export class RegisterFormComponent implements OnInit {

  @Input() system: string;

  constructor() { }

  ngOnInit(): void {
  }

  // Output event emitter
  @Output() registerResult: EventEmitter<any> = new EventEmitter();

  // Button click eventlistener
  onRegisterClicked() {
    this.registerResult.emit({
      registered: false,
      user: 'Cash',
      message: 'Registration attempted by: '
    });
  }

}
