# Task 30 - Angular Components
## - AngularFEdev
The task was as follows: 
*Create a Login Form component
*Create a Register Form component
*Both should have the following inputs:
*Username, Password
*The Register form should ALSO have Confirm Password
*Both components should have a button that emits a string to it’s parent.
*Both components should have an input that receives a string from it’s parent

Try to add the Login form and Register form component to the App Component. Check that it registers and that you can see the message received from the App COmponent and receive a message from the Child component (Login and Register)

## Limitations
Currently the app is not reading out the input fields. Expecting to learn more about form handling in future class.
Have done some attempts but need more instructions.
